
try:
    year = int(input("Enter year: "))
except:
    year = int(input("Enter valid year or it will cause an error: "))

if (year % 400 == 0) and (year % 100 == 0):
    print(f"{year} is a leap year")
elif (year % 4 ==0) and (year % 100 != 0):
    print(f"{year} is a leap year")
else:
    print(f"{year} is not a leap year")


        
row = int(input("Enter rows: "))
col = int(input("Enter columns: "))

for x in range(row):
    for y in range(col):
        print('*', end= " ")
    print ("\n")  
    